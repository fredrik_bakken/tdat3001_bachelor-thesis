#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 0.75in
\topmargin 1in
\rightmargin 0.75in
\bottommargin 0.75in
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Part*
Design Document
\end_layout

\begin_layout Section*
Elliptic Curve Cryptography
\end_layout

\begin_layout Subsection*
Version 1.0
\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Part*
Revisjonshistorie
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="4">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Date
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Version
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Description of Changes
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Author
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Part*
Table of Contents
\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Part*
Design Document
\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
Innledningen til dokumentet skal fortelle leseren hva som er hensikten med
 dokumentet og hva det inneholder.
 Han skal også forstå i hvilken forbindelse dette dokumentet er skrevet.
 Innledningen har følgende underpunkter:
\end_layout

\begin_layout Subsection
Purpose of the Document
\end_layout

\begin_layout Standard
Hensikten med dette dokumentet er å beskrive designet til systemet som er
 utviklet.
\end_layout

\begin_layout Subsection
Refinement
\end_layout

\begin_layout Standard
Her finner vi en kortfattet beskrivelse av det aktuelle prosjektet slik
 at det går klart frem hva dette dokumentet dekker.
 Hvis dokumentet dekker deler av systemet må det gå klart frem.
 Eventuelle relasjoner til andre systemer eller delsystemer beskrives.
\end_layout

\begin_layout Subsection
Definitions and Abbreviations
\end_layout

\begin_layout Standard
Alle begreper og forkortelser må defineres når dette er nødvendig for å
 forstå innholdet i dokumentet.
 NB! Husk at dette dokumentet skal leses også av brukerne.
\end_layout

\begin_layout Standard
Hvis det er laget en ordbok i prosjektet, kan det refereres her.
\end_layout

\begin_layout Subsection
References
\end_layout

\begin_layout Standard
Alle dokumenter som refereres andre steder i dette dokumentet skal listes
 opp her.
\end_layout

\begin_layout Subsection
List of Contents
\end_layout

\begin_layout Standard
Her forteller vi leserne hva dette dokumentet inneholder og hvordan det
 er organisert (hva han finner hvor).
\end_layout

\begin_layout Section
Background and Overview
\end_layout

\begin_layout Standard
Design vil si å realisere Use Case og tilhørende ikke-funksjonelle krav.
 Design er også en detaljering av arkitekturen til systemet.
 Henvis til aktuelt analysedokument og eventuell beskrivelse av arkitekturen.
 Vis i et diagram det logiske perspektivet for arkitekturen.
 Underpunktene som følger viser hvilke Use Case som er realisert.
\end_layout

\begin_layout Subsection
Use Case Model
\end_layout

\begin_layout Standard
Her viser vi det utsnittet av Use Case modellen som er realisert.
 Det kan gjøres ved hjelp av et diagram.
 Dette må suppleres med en beskrivelse av hvilke løp, hovedløp og eventuelle
 sideløp (relaterte løp) og unntak, som er realisert, og hva som gjenstår
 for senere iterasjoner eller versjoner av programvaren.
\end_layout

\begin_layout Subsection
Non-Functional Requirements
\end_layout

\begin_layout Standard
Her listes opp de ikkefunksjonelle kravene som er tilfredsstilt i designet.
 List også opp krav som ikke er tilfredsstilt.
 Ikkefunksjonelle krav er dokumentert i Tilleggsspesifikasjonen som det
 kan henvises til.
\end_layout

\begin_layout Subsection
Assumptions and Dependencies
\end_layout

\begin_layout Standard
Her beskriver vi så nøyaktig som mulig de forutsetningene som beskrivelsene
 i dette dokumentet bygger på.
 Videre beskriver vi så nøyaktig som mulig hvilke avhengigheter vi har,
 f.eks.
 til andre systemer.
\end_layout

\begin_layout Section
The Design
\end_layout

\begin_layout Standard
Her skal det følge en detaljert beskrivelse av designet som er utført.
 Her tar man for seg Use Case for Use Case og for hver Use Case, systemmelding
 for systemmelding.
 Designet illustreres med interaksjonsdiagrammer (sekvensdiagrammer).
 Diskuter tekstlig problemene knyttet til designet, hvilke beslutninger
 som ligger til grunn for designet og eventuell bruk av designmønstre1.
\end_layout

\begin_layout Subsection
Choice of Controls
\end_layout

\begin_layout Standard
Diskuter valg av kontroller.
 Hvis det brukes egne kontrollere for hvert Use Case, henvises det til diskusjon
 i tilknytning til hvert Use Case.
\end_layout

\begin_layout Subsection
Use Case 1
\end_layout

\begin_layout Standard
Henvis til aktuelt Use Case i krav-/arkitekturdokumentet.
 Beskriv hvilke løp som er realisert.
 Vis til-hørende system sekvensdiagram.
 Diskuter valg av kontroller.
\end_layout

\begin_layout Subsubsection
System Message 1.1
\end_layout

\begin_layout Standard
Beskriv designet som er gjort for denne systemmeldingen.
 Diskuter problemene knyttet til designet av denne systemmeldingen, hvilke
 vurderinger som er gjort, hvilke objekter som er hentet fra domene-modellen
 og eventuell bruk av designmønstre for å finne en løsning på problemet.
 Illustrer designet med interaksjonsdiagrammer (sekvensdiagram eller kommunikasj
onsdiagram3).
\end_layout

\begin_layout Subsubsection
System Message 1.2
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Subsection
Use Case 2
\end_layout

\begin_layout Subsubsection
System Message 2.1
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Subsubsection
System Message 2.2
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Section
Design Class Model
\end_layout

\begin_layout Standard
Etter at alle Use Case er designet vil man ha kommet frem til de objektene
 som skal realisere Use Case-ene.
 Disse må samles i en designklassemodell som illustreres i et overordnet
 klassediagram.
 For store systemer kan det bli nødvendig å dele opp i flere diagrammer
 for ikke å miste oversikten.
 På overordnet nivå kan dette illustreres med pakkediagrammer.
 Hver klasse og interface må dokumenteres med sitt navn, attributter og
 operasjoner og plass i et eventuelt klassehierarki.
\end_layout

\begin_layout Subsection
Parent Class Model
\end_layout

\begin_layout Standard
Vis den overordnede designklassemodellen i et UML klassediagram.
 For store systemer kan det være aktuelt med diagrammer på flere nivå for
 å vise subsystemer og grupperinger i pakker.
\end_layout

\begin_layout Subsection
Detailed Description of Class A
\end_layout

\begin_layout Standard
Vis den
\end_layout

\begin_layout Standard
Type klasse (abstrakt, aktiv, konkret)
\end_layout

\begin_layout Standard
Pakke klassen tilhører
\end_layout

\begin_layout Standard
Plass i arvhierarki, superklasser, interface som realiseres
\end_layout

\begin_layout Standard
Relasjoner, roller i assosiasjoner, multiplisitet
\end_layout

\begin_layout Standard
Attributter
\end_layout

\begin_layout Standard
Operasjoner
\end_layout

\begin_layout Standard
Spesielle ting
\end_layout

\begin_layout Standard
Visualiser med egnede UML-diagrammer
\end_layout

\begin_layout Subsection
Detailed Description of Class B
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Subsection
Detailed Description of the Interface I1
\end_layout

\begin_layout Standard
Pakke interfacet tilhører
\end_layout

\begin_layout Standard
Plass i arvhierarki
\end_layout

\begin_layout Standard
Konstanter
\end_layout

\begin_layout Standard
Operasjoner
\end_layout

\begin_layout Standard
Spesielle ting
\end_layout

\begin_layout Subsection
Detailed Description of the Interface I2
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Section
Additional Information
\end_layout

\begin_layout Standard
Evt.
 annen informasjon som vi ønsker å ta med for å øke forståeligheten og tilgjenge
ligheten av det som er dokumentert.
\end_layout

\end_body
\end_document
