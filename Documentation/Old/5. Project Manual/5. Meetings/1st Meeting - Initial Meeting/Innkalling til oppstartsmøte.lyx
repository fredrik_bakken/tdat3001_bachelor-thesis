#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 0.75in
\topmargin 1in
\rightmargin 0.75in
\bottommargin 0.75in
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard

\series bold
Invitert til møtet:
\end_layout

\begin_layout Standard
Arne Aas (Atmel Norway) og Ole Christian Eidheim (NTNU)
\end_layout

\begin_layout Standard
\align right
NTNU Kalvskinnet | 6.
 Januar 2016
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf

\series bold
\size largest
Innkalling til oppstartsmøte
\end_layout

\begin_layout Standard
\noindent
Det innkalles med dette til oppstartsmøte for bacheloroppgave, E24 - Elliptic
 Curve Cryptography.
\end_layout

\begin_layout Section*

\series bold
Sted for møtet
\end_layout

\begin_layout Standard
Dato: Torsdag 7.
 Januar 2016
\end_layout

\begin_layout Standard
Klokken: 11.00
\end_layout

\begin_layout Standard
Sted: Atmel Norway, Vestre Rosten 79, 7075 Tiller, 
\begin_inset Quotes eld
\end_inset

Solsiden
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Section*

\series bold
Agenda/saksliste
\end_layout

\begin_layout Enumerate
Åpning av møtet
\end_layout

\begin_layout Enumerate
Valg av referent (Fredrik Bakken)
\end_layout

\begin_layout Enumerate
Godkjenning av innkalling og saksliste
\end_layout

\begin_layout Enumerate
Drøft oppgaven (prosjektet) og forsøke å trekke opp rammene (Vedlegg 1)
\end_layout

\begin_deeper
\begin_layout Enumerate
Foreløpig møteplan
\end_layout

\begin_layout Enumerate
Inndeling av prosjektet i deler
\end_layout

\begin_layout Enumerate
Fordeling av ansvar mellom deltakerne
\end_layout

\begin_layout Enumerate
Fremdriftsplan for prosjektet
\end_layout

\begin_layout Enumerate
Valg av utviklingsprosess
\end_layout

\begin_layout Enumerate
Krav til dokumentasjon
\end_layout

\end_deeper
\begin_layout Enumerate
Gå gjennom prosjekthåndboken
\end_layout

\begin_layout Enumerate
Poengter at prosjektframdriften er studentens ansvar
\end_layout

\begin_layout Enumerate
Gjennomgang av retningslinjene for vurdering
\end_layout

\begin_layout Enumerate
Bevisstgjøring av ambisjonsnivå
\end_layout

\begin_layout Enumerate
Orienter om opphavsrettigheter og skolens ansvarsfraskrivelse
\end_layout

\begin_layout Enumerate
Eksterne oppgaver: Skriv under kontraktene
\end_layout

\begin_layout Enumerate
Bruk av It's Learning eller andre verktøy for prosjektoppfølging og samarbeid
\end_layout

\begin_layout Enumerate
Valg av språk
\end_layout

\begin_deeper
\begin_layout Enumerate
Gjennomgående norsk
\end_layout

\begin_layout Enumerate
Gjennomgående engelsk
\end_layout

\begin_layout Enumerate
Blanding, kode på engelsk, dokumentasjon på norsk
\end_layout

\end_deeper
\begin_layout Enumerate
Referatet fra oppstartsmøtet
\end_layout

\begin_deeper
\begin_layout Enumerate
Der inngåes avtaler som gjelder for oppgaven/prosjektet som helhet
\end_layout

\begin_layout Enumerate
Veileder skal godkjenne referatet
\end_layout

\end_deeper
\begin_layout Standard
For en utdypende agenda/saksliste, se vedlegg 3.
\end_layout

\begin_layout Section*
\begin_inset Newpage newpage
\end_inset

Vedlegg 1: Oppgavebeskrivelse
\end_layout

\begin_layout Subsection*
Arbeidstittel:
\end_layout

\begin_layout Standard
Elliptic Curve Cryptography (ECC)
\end_layout

\begin_layout Subsection*
Hensikt
\end_layout

\begin_layout Standard
Gain an understanding of elliptic curve cryptography (ECC) and side channel
 attacks, implement some ECC algorithms and contribute with side channel
 protection mechanisms.
\end_layout

\begin_layout Subsection*
Beskrivelse
\end_layout

\begin_layout Itemize
Study elliptic curve cryptography theory
\end_layout

\begin_layout Itemize
Study known side channel attack methods
\end_layout

\begin_layout Itemize
Study existing work on elliptic curve cryptography
\end_layout

\begin_layout Itemize
Implement ECC encrypt and decrypt algorithms in C
\end_layout

\begin_layout Itemize
Add side channel protection mechanisms against timing and power analysis
\end_layout

\begin_layout Itemize
Discuss the effectiveness of the proposed mechanisms considering the effects
 of various implementations that can be chosen by a compiler.
 List attack surfaces that cannot easily be covered at the C level
\end_layout

\begin_layout Subsection*
Stiller arbeidsplass
\end_layout

\begin_layout Standard
Ja
\end_layout

\begin_layout Subsection*
Antall studenter
\end_layout

\begin_layout Standard
1
\end_layout

\begin_layout Subsection*
Antall dager
\end_layout

\begin_layout Standard
50
\end_layout

\begin_layout Standard
Eventuelle studenter bestemt for oppgaven: Fredrik Bakken, 3ING DATA
\end_layout

\begin_layout Subsection*
Oppgavestiller
\end_layout

\begin_layout Standard
Atmel Norway
\end_layout

\begin_layout Subsection*
Adresse
\end_layout

\begin_layout Standard
Vestre Rosten 79
\end_layout

\begin_layout Standard
7075 Tiller
\end_layout

\begin_layout Subsection*
Kontaktperson
\end_layout

\begin_layout Standard
Arne Aas
\end_layout

\begin_layout Standard
Tlf: 72884388
\end_layout

\begin_layout Standard
Fax: 72884399
\end_layout

\begin_layout Standard
E-mail: arne.aas@atmel.com
\end_layout

\begin_layout Section*
\begin_inset Newpage newpage
\end_inset

Vedlegg 2: Prosjekthåndboka
\end_layout

\begin_layout Itemize
Fremdriftsplan - Gantt-diagram
\end_layout

\begin_deeper
\begin_layout Itemize
Revisjoner skal inn i prosjekthåndboka
\end_layout

\end_deeper
\begin_layout Itemize
Eventuelt arbeidskontrakt
\end_layout

\begin_layout Itemize
Timelister hver uke med timer fordelt på person og aktivitet
\end_layout

\begin_layout Itemize
Ukerapporter fordelt på person Omfang 20sp ≈ 500t inkl.
 vitenskapsteori og -metode
\end_layout

\begin_deeper
\begin_layout Itemize
La vitenskapsteori og -metode være en aktivitet som går over uke 1-3 i Gantt-dia
grammet
\end_layout

\end_deeper
\begin_layout Itemize
Timene kan f.eks.
 fordeles slik:
\end_layout

\begin_deeper
\begin_layout Itemize
28 t/uke f.o.m.
 uke 1 i ca.
 18 arbeidsuker
\end_layout

\begin_layout Itemize
Det er trukket fra to uker for påske, helligdager og eksamen
\end_layout

\begin_layout Itemize
Kan justeres ukevis i forhold til belastningen i emnet TDAT3002
\end_layout

\end_deeper
\begin_layout Itemize
Møteinnkallinger og -referat
\end_layout

\begin_deeper
\begin_layout Itemize
Referatet fra oppstartsmøtet skal godkjennes av veileder
\end_layout

\begin_layout Itemize
Møter med veileder skal alltid ha både innkalling og referat
\end_layout

\begin_layout Itemize
Andre møter etter behov
\end_layout

\end_deeper
\begin_layout Section*
\begin_inset Newpage newpage
\end_inset

Vedlegg 3: Utdypende agenda/saksliste
\end_layout

\begin_layout Enumerate

\series bold
Åpning av møtet
\end_layout

\begin_deeper
\begin_layout Standard
Møtet åpnes med å ønske alle inviterte velkommen til oppstartsmøte for bacheloro
ppgave, E24 - Elliptic Curve Cryptography.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Valg av referent
\end_layout

\begin_deeper
\begin_layout Standard
Et møte har alltid en møteleder som styrer møtet og en referent som noterer
 alle punkter som nevnes og hva som blir konkludert.
 Siden prosjektet gjennomføres som et enkeltmannsprosjekt, så vil studenten
 notere ned avgjørelser og hendelser samtidig som diskusjonen pågår.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Godkjenning av innkalling og saksliste
\end_layout

\begin_deeper
\begin_layout Standard
Har alle inviterte mottatt og lest over møteinnkallelsen og vedleggene?
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Drøft oppgaven (prosjektet) og forsøke å trekke opp rammene (Vedlegg 1)
\end_layout

\begin_deeper
\begin_layout Standard
Gjennomgår innholdet i oppgavebeskrivelsen og trekker fram spørsmål rettet
 til hvordan oppgaven kan løses.
 Orienterer også om at det vil være mulig for oppgavestiller å komme med
 flere arbeidskrav utover i prosjektperioden.
\end_layout

\begin_layout Enumerate

\series bold
Foreløpig møteplan
\end_layout

\begin_deeper
\begin_layout Standard
Informerer både oppgavestiller og veileder om at oppgavestiller vil være
 tilstede under oppstartsmøtet, samt et senere møte knyttet til presentasjonen
 og overlevering av det endelige produktet.
\end_layout

\begin_layout Standard
Studenten kommer med forslag om at det i starten legges opp til jevne møter
 med veileder hver 2.
 uke i starten og deretter går over til hver møter hver 3.
 uke eller etter behov.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Inndeling av prosjektet i deler
\end_layout

\begin_deeper
\begin_layout Standard
Et foreløpig forslag til oppdeling av prosjektet er presentert gjennom et
 Gantt-diagram som studenten bringer med seg til møtet.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Fordeling av ansvar mellom deltakerne
\end_layout

\begin_deeper
\begin_layout Standard
Studenten er selv ansvarlig for fremdriften i prosjektet, holde tidsfrister
 og å ha et ferdig og endelig produkt ved enden av prosjektperioden.
\end_layout

\begin_layout Standard
Forslag til at ansvaret til oppgavestiller ligger på å komme med flere arbeidskr
av dersom det sees på som nødvendig at studenten bør være innen flere relevante
 temaer.
\end_layout

\begin_layout Standard
Forslag til at ansvaret til veileder ligger på å være behjelpelig ved større
 problemstillinger som studenten potensielt møter i oppgaven.
 Ellers vil studenten være selvstendig og må løse alle arbeidsoppgaver på
 egenhånd.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Fremdriftsplan for prosjektet
\end_layout

\begin_deeper
\begin_layout Standard
Se Gantt-diagrammet for fremdriftsplanen som ble nevnt i punkt 4b.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Valg av utviklingsprosess
\end_layout

\begin_deeper
\begin_layout Standard
Forslag til at arbeidsprosessen som brukes i utviklingsprosessen av prosjektet
 skal være SCRUM.
 Forslaget om å bruke SCRUM gjøres på grunnlag av at man gjennom gitte sprinter
 har kommet fram til definerte delmål som skal ferdigstilles i løpet av
 sprintperiodene.
 På denne måten vil man hele tiden ha delt opp delmålene i mindre oppgaver
 som det enkelt å følge hele utviklingsprosessen.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Krav til dokumentasjon
\end_layout

\begin_deeper
\begin_layout Standard
De kravene som gjelder for dokumentasjon er en hovedrapport, som ut ifra
 omfang skal ligge på mellom 20-40 sider, samt vedlegg.
 Legger inn et forslag til at omfanget på mellom 20-40 sider kan komme til
 å være for lite, ettersom dette prosjektet i hovedsak baserer seg mer på
 et forskningsprosjekt kontra et utviklingsprosjekt av et system.
 På bakgrunn av dette kommer ikke vedleggene til å bli like omfattende som
 om det skulle vært et storskala system som hadde blitt utviklet.
 Forslaget er dermed å øke omfanget med mer enn 20-40 sider.
\end_layout

\begin_layout Standard
I tillegg til hovedrapporten kommer vedleggsdokumenter som forstudierapport/visj
onsdokument, prosjekthåndbok, permanent dokumentasjon, og relevante deler
 av underveisdokumentasjon.
\end_layout

\begin_layout Standard
For underveisdokumentasjon gjelder krav-, design-, arkitektur-, og test-dokument
asjon.
\end_layout

\begin_layout Standard
For permanent dokumentasjon gjelder system-, system- og driftsdokumentasjon.
 Eventuelt kan disse leveres som ett dokument, eller være ei nettside.
\end_layout

\begin_layout Standard
I tillegg til denne dokumentasjonen, legges det inn krav til dokumentasjon
 for møteinnkallelser og referater, hvor referatene skal godkjennes av veileder.
\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate

\series bold
Gå gjennom prosjekthåndboken (Vedlegg 2)
\end_layout

\begin_deeper
\begin_layout Standard
Prosjekthåndboken er en fremdriftsoversikt med fremdriftsplan i form av
 et Gantt-diagram, frivillig(e) arbeidskontrakt(er), timelister, ukerapporter,
 samt møteinnkallinger og -referater.
 Studenten viser foreløpig oversikt over fremdriftsoversikten.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Poengter at prosjektframdriften er studentens ansvar
\end_layout

\begin_deeper
\begin_layout Standard
Som tidligere nevnt under punkt 4c.
 er det studenten selv som er ansvarlig for prosjektframdriften.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Gjennomgang av retningslinjene for vurdering
\end_layout

\begin_deeper
\begin_layout Standard
Fra emnebeskrivelsen er følgende vurdering gitt: 
\shape italic
Prosess, produkt og rapporter teller.
 Alle tre delene må være bestått hver for seg
\shape default
.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
Produktet Dette betyr det programsystemet dere har laget, inkludert permanent
 dokumentasjon.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
Prosessen Dette omfatter måten dere gjennomfører den valgte utviklingsprosessen
 på, samt ryddighet med hensyn på timelister, møter og lignende.
 Samsvar mellom planer og resultater inngår også her.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
Rapporter Her vurderes hovedrapporten med vedlegg.
 Forstudierapporten (ev.
 visjonsdokumentet) teller også med her.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Bevisstgjøring av ambisjonsnivå
\end_layout

\begin_deeper
\begin_layout Standard
Eget ambisjonsnivå er at jeg ønsker å gjøre en så god jobb som mulig og
 dermed virkelig sette meg inn i temaet og skaffe til veie en bred kunnskap
 om temaet.
 På bakgrunn av dette håper jeg at dette skal holde til en karakter B med
 håp om å kunne strekkes til en karakter A.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Orienter om opphavsrettigheter og skolens ansvarsfraskrivelse
\end_layout

\begin_deeper
\begin_layout Standard
Oppgavestiller skal ha fått tilsendt dokumentasjon om opphavsrettigheter
 og skolens ansvarsfraskrivelse som skal orientere om hvordan skolen stiller
 seg til opphavsrettigheter, tilgjengelighet og ansvarsfraskrivelse.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Eksterne oppgaver: Skriv under kontraktene
\end_layout

\begin_deeper
\begin_layout Standard
Skriver under på alle kontrakter som gjelder.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Bruk av It’s Learning eller andre verktøy for prosjektoppfølging og samarbeid
\end_layout

\begin_deeper
\begin_layout Standard
Forslag til bruk av følgende verktøy under prosjektarbeidet og prosjektoppfølgin
gen:
\end_layout

\begin_layout Itemize

\shape italic
Microsoft Project
\shape default
: For prosjektstyring, fremdriftsplaner, timerapporteringer, mm.
\end_layout

\begin_layout Itemize

\shape italic
Lyx (LaTeX)
\shape default
: For dokumentasjonsarbeid
\end_layout

\begin_layout Itemize

\shape italic
CodeBlocks
\shape default
 og 
\shape italic
Atmel Studio
\shape default
: For kompilering og kjøring av programkode
\end_layout

\begin_layout Itemize

\shape italic
Git (BitBucket)
\shape default
 og 
\shape italic
TortoiseSVN
\shape default
: For versjonskontroll og arbeidshistorikk
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Valg av språk
\end_layout

\begin_deeper
\begin_layout Standard
Forslag til bruk av gjennomgående engelsk på alle dokumenter, bortsett fra
 møteinnkallelser og møtereferater.
 Dette på bakgrunn av at det som oftest er enklere å gjengi det som blir
 sagt under møter på norsk, istedenfor å potensielt gi feil formulering
 ved å måtte gjengi det som blir sagt på norsk til engelsk.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Referat fra oppstartsmøtet
\end_layout

\begin_deeper
\begin_layout Standard
Referat fra møtet vil ferdigstilles så raskt som mulig av studenten før
 det videresendes til veileder for godkjenning.
 Er det ønskelig at studenten kommer innom for å få referatet godkjent og
 skrevet under, eller kan dette gjøres via epost?
\end_layout

\end_deeper
\end_body
\end_document
