#Project Planning and Agile Management with SCRUM

##SCRUM Methodology
To correctly use the SCRUM methodology in this project, I have used the resources found in the at the following website: http://scrummethodology.com/

For my own Bachelor Thesis, see the following website for the current project status (simplified version): http://www.bitbucketcards.com/fredrik_bakken/tdat3001_bachelor-thesis#

##SCRUM
Every sprint contains some form of the following phases:

- Requirements Analysis
- Design
- Code
- Integration
- Test
- Deploy

The goal is to make a shippable product every sprint, no matter how small it is. This process follow the feedback process, where after every sprint the feedback defines a better understanding of the end-product.

##Roles

- Product Owner
- SCRUM Master
- SCRUM Development Team

##Product Backlog

- Every feature defined by the Product Owner
- Product Owner priorities the features defined in the Product Backlog
- Written in User Stories or Use-Cases

##Sprint Backlog

- The features that has been commited to do during the current sprint

| Commited Backlog Item	| Not Started	| In Progress	| Completed	|