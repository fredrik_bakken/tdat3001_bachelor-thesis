\contentsline {section}{Revision History}{i}{section*.1}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Purpose}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Scope}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Definitions, Acronyms, and Abbreviations}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}References}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Overview}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Background and Overview}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Use Case Model}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Encrypt Data}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Decrypt Data}{6}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Non-Functional Requirements}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Assumptions and Dependencies}{6}{section.2.3}
\contentsline {chapter}{\numberline {3}Architecture Description}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Logical Perspective}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Process Perspective}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Deployment Perspective}{7}{section.3.3}
\contentsline {section}{\numberline {3.4}Implementation Perspective}{8}{section.3.4}
\contentsline {section}{\numberline {3.5}Other Perspectives}{8}{section.3.5}
\contentsline {chapter}{\numberline {4}Additional Information}{9}{chapter.4}
