\contentsline {section}{Revision History}{i}{section*.1}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Purpose}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Scope}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Definitions, Acronyms, and Abbreviations}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}References}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Overview}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Background and Overview}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Use Case Model}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Non-Functional Requirements}{5}{section.2.2}
\contentsline {chapter}{\numberline {3}The Design}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Controller}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Use Case 1}{6}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}System Event 1.1}{6}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}System Event 1.2}{6}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Use Case 2}{7}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}System Event 2.1}{7}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}System Event 2.2}{7}{subsection.3.3.2}
\contentsline {chapter}{\numberline {4}The Design Class Model}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Conceptual Design Class Model}{8}{section.4.1}
\contentsline {section}{\numberline {4.2}A Detailed Description of Object Class A}{8}{section.4.2}
\contentsline {section}{\numberline {4.3}A Detailed Description of Object Class B}{8}{section.4.3}
\contentsline {section}{\numberline {4.4}A Detailed Description of Interface I1}{8}{section.4.4}
\contentsline {section}{\numberline {4.5}A Detailed Description of Interface I2}{9}{section.4.5}
\contentsline {chapter}{\numberline {5}Additional Information}{10}{chapter.5}
