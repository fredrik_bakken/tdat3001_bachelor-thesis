\contentsline {section}{Revision History}{i}{section*.1}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Purpose}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Scope}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Definitions, Acronyms, and Abbreviations}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}References}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Overview}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Background for the Project}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Description of the Problems and Needs}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Understanding the Current System and Procedures}{7}{section.2.2}
\contentsline {chapter}{\numberline {3}Project Goals}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Efficiency Goals}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Result Goals}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}Process Goals}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}The Project Scope}{9}{section.3.4}
\contentsline {section}{\numberline {3.5}Project Milestones and Main Activities}{10}{section.3.5}
\contentsline {chapter}{\numberline {4}Stakeholder and Conditions}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Stakeholder Analysis}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Regulatory Framework}{12}{section.4.2}
\contentsline {chapter}{\numberline {5}Critical Success Factors}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Success Factors}{13}{section.5.1}
\contentsline {section}{\numberline {5.2}Information Needs}{14}{section.5.2}
\contentsline {chapter}{\numberline {6}Risk Analysis}{15}{chapter.6}
\contentsline {chapter}{\numberline {7}Cost/Benefit Analysis}{18}{chapter.7}
\contentsline {section}{\numberline {7.1}Quantifiable and Non-quantifiable Benefits}{18}{section.7.1}
\contentsline {section}{\numberline {7.2}Loss of Direct Costs}{19}{section.7.2}
\contentsline {section}{\numberline {7.3}Estimated Costs}{19}{section.7.3}
\contentsline {section}{\numberline {7.4}Compare Costs and Benefits}{19}{section.7.4}
\contentsline {chapter}{\numberline {8}Guidelines and Standards}{20}{chapter.8}
\contentsline {section}{\numberline {8.1}Documentation Requirements}{20}{section.8.1}
\contentsline {section}{\numberline {8.2}Requirements for Quality Reviews}{21}{section.8.2}
\contentsline {section}{\numberline {8.3}Requirements for Standards and Methods}{21}{section.8.3}
\contentsline {section}{\numberline {8.4}Change Management}{21}{section.8.4}
\contentsline {chapter}{\numberline {9}Project Organization}{23}{chapter.9}
\contentsline {chapter}{\numberline {10}Recommendations for Further Work}{24}{chapter.10}
\contentsline {chapter}{\numberline {A}Progress Overview}{25}{appendix.A}
