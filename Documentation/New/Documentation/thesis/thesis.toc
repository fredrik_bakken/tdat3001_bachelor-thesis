\contentsline {section}{Preface}{i}{Doc-Start}
\contentsline {section}{Acknowledgment}{ii}{Doc-Start}
\contentsline {section}{Project Description}{iii}{Doc-Start}
\contentsline {section}{Summary and Conclusions}{iv}{Doc-Start}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Limitations}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Approach}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Structure of the Report}{5}{section.1.5}
\contentsline {chapter}{\numberline {2}Introduction to Cryptography}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Simple Equations}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Including Figures}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Including Tables}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Copying Figures and Tables}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}References to Figures and Tables}{11}{section.2.5}
\contentsline {section}{\numberline {2.6}A Word About Font-encoding}{11}{section.2.6}
\contentsline {section}{\numberline {2.7}Plagiarism}{12}{section.2.7}
\contentsline {chapter}{\numberline {3}Summary}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Summary and Conclusions}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Discussion}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Recommendations for Further Work}{14}{section.3.3}
\contentsline {chapter}{\numberline {A}Acronyms}{15}{appendix.A}
\contentsline {chapter}{\numberline {B}Project Documentation}{16}{appendix.B}
\contentsline {section}{\numberline {B.1}Vision Document}{16}{section.B.1}
\contentsline {section}{\numberline {B.2}Along Documentation}{44}{section.B.2}
\contentsline {subsection}{\numberline {B.2.1}Requirements Document}{44}{subsection.B.2.1}
\contentsline {subsection}{\numberline {B.2.2}Software Architecture Document}{61}{subsection.B.2.2}
\contentsline {subsection}{\numberline {B.2.3}Software Design Document}{72}{subsection.B.2.3}
\contentsline {subsection}{\numberline {B.2.4}Test Document}{85}{subsection.B.2.4}
\contentsline {section}{\numberline {B.3}Permanent Documentation}{107}{section.B.3}
\contentsline {subsection}{\numberline {B.3.1}Operating Document}{107}{subsection.B.3.1}
\contentsline {subsection}{\numberline {B.3.2}System Document}{107}{subsection.B.3.2}
\contentsline {subsection}{\numberline {B.3.3}User Document}{107}{subsection.B.3.3}
\contentsline {section}{\numberline {B.4}Project Manual}{107}{section.B.4}
\contentsline {subsection}{\numberline {B.4.1}Progress Overview}{107}{subsection.B.4.1}
\contentsline {subsection}{\numberline {B.4.2}Contracts}{107}{subsection.B.4.2}
\contentsline {subsection}{\numberline {B.4.3}Timesheet}{107}{subsection.B.4.3}
\contentsline {subsection}{\numberline {B.4.4}Week Reports}{134}{subsection.B.4.4}
\contentsline {subsection}{\numberline {B.4.5}Meetings}{145}{subsection.B.4.5}
\contentsline {subsubsection}{1st Meeting - Invitation}{145}{subsection.B.4.5}
\contentsline {subsubsection}{1st Meeting - Minute}{155}{subsection.B.4.5}
\contentsline {subsubsection}{2nd Meeting - Invitation}{163}{subsection.B.4.5}
\contentsline {subsubsection}{2nd Meeting - Minute}{165}{subsection.B.4.5}
\contentsline {subsubsection}{3rd Meeting - Invitation}{169}{subsection.B.4.5}
\contentsline {subsubsection}{3rd Meeting - Minute}{171}{subsection.B.4.5}
\contentsline {subsubsection}{4th Meeting - Invitation}{175}{subsection.B.4.5}
\contentsline {subsubsection}{4th Meeting - Minute}{175}{subsection.B.4.5}
\contentsline {subsubsection}{5th Meeting - Invitation}{175}{subsection.B.4.5}
\contentsline {subsubsection}{5th Meeting - Minute}{175}{subsection.B.4.5}
\contentsline {subsubsection}{6th Meeting - Invitation}{175}{subsection.B.4.5}
\contentsline {subsubsection}{6th Meeting - Minute}{175}{subsection.B.4.5}
\contentsline {subsubsection}{7th Meeting - Invitation}{175}{subsection.B.4.5}
\contentsline {subsubsection}{7th Meeting - Minute}{175}{subsection.B.4.5}
\contentsline {chapter}{Bibliography}{176}{subsection.B.4.5}
\contentsline {chapter}{Curriculum Vitae}{177}{appendix*.7}
