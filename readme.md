#Bachelor Thesis in Computer Engineering

##Topic Description
The topic description for the subject TDAT3001 - Bachelor Thesis in Computer Engineering can be found here: https://www.emweb.no/hist/node/560/4521

##Task Description
Gain an understanding of elliptic curve cryptography (ECC) and side channel attacks, implement some ECC algorithms and contribute with side channel protection mechanisms.

- Study elliptic curve cryptography theory
- Study known side channel attack methods
- Study existing work on elliptic curve cryptography
- Implement ECC encrypt and decrypt algorithms in C
- Add side channel protection mechanisms against timing and power analysis
- Discuss the effectiveness of the proposed mechanisms considering the effects of various implementations that can be chosen by a compiler. List attack surfaces that cannot easily be covered at the C level

##Suggested Literature to Study
### Elliptic Curve Cryptography

- [Understanding Cryptography: A Textbook for Students and Practitioners](http://www.crypto-textbook.com/)
- [Elliptic Curves: Number Theory and Cryptography, Second Edition](https://www.crcpress.com/Elliptic-Curves-Number-Theory-and-Cryptography-Second-Edition/Washington/9781420071467)
- [Applied Cryptography: Protocols, Algorithms, and Source Code in C](https://www.schneier.com/books/applied_cryptography/)

###Side-Channel Attacks

- [Hardware Security: Design, Threats, and Safeguards](https://www.crcpress.com/Hardware-Security-Design-Threats-and-Safeguards/Mukhopadhyay-Chakraborty/9781439895832)
- [Power Analysis Side Channel Attacks: The Processor Design-level Context](http://www.amazon.com/Power-Analysis-Side-Channel-Attacks/dp/3836485087)

###Other Literature and Research Papers

- [Software Implementations of Elliptic Curve Cryptography](http://ijns.jalaxy.com.tw/download_paper.jsp?PaperID=IJNS-2006-09-13-3&PaperName=ijns-v7-n1/ijns-2008-v7-n1-p141-150.pdf)